flectra.define('web.WebClient', function (require) {
"use strict";

var core = require('web.core');
var AbstractWebClient = require('web.AbstractWebClient');
var data_manager = require('web.data_manager');
var session = require('web.session');
var ActionManager = require('web.ActionManager');
var rpc = require('web.rpc');

var AppDashboard = require('web.AppDashboard');
var Menu = require('web.BackendThemeMenu');

return AbstractWebClient.extend({
    custom_events: _.extend({}, AbstractWebClient.prototype.custom_events, {
        on_menu_action: 'on_menu_action',
        _onClickMenuToggle: '_onClickMenuToggle',
        show_app_dashboard:'toggle_dashboard',
    }),
    toggle_dashboard:function () {
        this.app_dashboard_on_off(true);
    },
    start: function () {
        var self = this;
        core.bus.on('update_menu_id_url', this, function (menu_id) {
            self.do_push_state(_.extend($.bbq.getState(), {
                menu_id: menu_id
            }));
        });
        return this._super.apply(this, arguments);
    },
    load_menus: function () {
        return rpc.query({
            model: 'ir.ui.menu',
            method: 'load_menus',
            args: [core.debug],
            kwargs: {context: session.user_context},
        }).then(function (menu_data) {
            for (var i = 0; i < menu_data.children.length; i++) {
                var child = menu_data.children[i];
                if (child.action === false) {
                    while (child.children && child.children.length) {
                        child = child.children[0];
                        if (child.action) {
                            menu_data.children[i].action = child.action;
                            break;
                        }
                    }
                }
            }
            return menu_data;
        });
    },
    set_action_manager: function () {
        this.action_manager = new ActionManager(this, {webclient: this});
        return this.action_manager.appendTo(this.$el.find('.o_main_content'));
    },
    show_application: function() {
        var self = this;
        return this._initializeWidget().then(function () {
            $(window).bind('hashchange', self.on_hashchange);
            if (_.isEmpty($.bbq.getState(true))) {
                self._appDashboardOnOff(true);
            } else {
                return self.on_hashchange();
            }
        });

    },
    on_hashchange: function(event) {
        if (this._ignore_hashchange) {
            this._ignore_hashchange = false;
            return;
        }

        var self = this;
        return this.clear_uncommitted_changes().then(function () {
            self._updatePrevHash();
            var stringstate = $.bbq.getState(false);
            if (!_.isEqual(self._current_state, stringstate)) {
                var state = $.bbq.getState(true);
                if (state.action || (state.model && (state.view_type || state.id))) {
                    state._push_me = false;
                    return self.action_manager.do_load_state(state, !!self._current_state).then(function () {
                        if (state.menu_id) {
                            if (state.menu_id !== self.menu.current_primary_menu) {
                                core.bus.trigger('update_menu_id_url', state.menu_id);
                            } else if (!state.view_type || !state.model) {
                                self.on_menu_action({
                                    data: {
                                        menu_id: state.menu_id,
                                        action_id: state.action
                                    }
                                });
                            }
                        } else {
                            var action = self.action_manager.get_inner_action();
                            if (action) {
                                var menu_id = self.menu._actionIdToMenuId(action.get_action_descr().id);
                                if (menu_id) {
                                    core.bus.trigger('update_menu_id_url', menu_id);
                                }
                            }
                        }
                        self._appDashboardOnOff(false);
                    });
                } else if (state.menu_id) {
                    var action_id = self.menu._menuIdToActionId(state.menu_id);
                    return self.do_action(action_id, {clear_breadcrumbs: true}).then(function () {
                        core.bus.trigger('update_menu_id_url', state.menu_id);
                        self._appDashboardOnOff(false);
                    });
                } else {
                    self._appDashboardOnOff(true);
                }
            }
            self._current_state = stringstate;

        }, function () {
            if (event) {
                self._ignore_hashchange = true;
                window.location = event.originalEvent.oldURL;
            }
        });
    },
    _initializeWidget: function () {
        var self = this;
        var defs = [];
        return self.load_menus().then(function (menu_data) {
            if (self.app_dashboard) {
                self.app_dashboard.destroy();
            }
            if (self.menu) {
                self.menu.destroy();
            }
            self.menu_data = menu_data;
            self.app_dashboard = new AppDashboard(self, menu_data);
            self.menu = new Menu(self, menu_data);
            defs.push(self.menu.prependTo(self.$el));
            return $.when.apply($, defs);
        });
    },
    on_menu_action: function(ev) {
        var self = this;
        return this.menu_dm.add(data_manager.load_action(ev.data.action_id))
            .then(function (result) {
                return self.action_mutex.exec(function () {
                    var completed = $.Deferred();
                    $.when(self.do_action(result, {
                        clear_breadcrumbs: true,
                        action_menu_id: ev.data.menu_id,
                    })).fail(function () {
                        self._appDashboardOnOff(true);
                        completed.resolve();
                    }).done(function () {
                        core.bus.trigger('update_menu_id_url', ev.data.menu_id);
                        self._appDashboardOnOff(false);
                        completed.resolve();
                    });
                    return completed;
                });
            });
    },
    do_push_state: function (state) {
        if (!state.menu_id && this.menu) {
            state.menu_id = this.menu.current_primary_menu;
        }
        this._super.apply(this, arguments);
    },
    _onClickMenuToggle: function (e) {
        e.data.preventDefault();
        if (this.app_dashboard_display) {
            $.bbq.pushState(this.prev_hash, 2);
            this._appDashboardOnOff(false);
        } else {
            this._appDashboardOnOff(true);
        }

    },
    _appDashboardOnOff: function (status) {
        // status : on(true)/off(false)
        if (status) {
            var self = this;
            this.clear_uncommitted_changes().then(function () {
                self.app_dashboard.appendTo($('.o_web_client'));
                $.bbq.pushState('#home', 2);
                if (self.is_prev_hash) {
                    self.$el.find('.o_menu_toggle').removeClass('o_hidden').addClass('fa-chevron-left').removeClass('fa-th');
                } else {
                    self.$el.find('.o_menu_toggle').addClass('o_hidden');
                }
                self.$el.find('.o_main').addClass('o_hidden');
                self.$el.find('.flectra_app_switcher').removeClass('o_hidden');
                self.$el.find('.o_menu_sections').addClass('o_hidden');
                self.$el.find('.o_menu_brand').addClass('o_hidden');
                self.$el.find('.o_mobile_menu_toggle').addClass('o_hidden');
                self.app_dashboard_display = true;
            });
        } else {
            this.$el.find('.o_menu_toggle').addClass('fa-th').removeClass('fa-chevron-left').removeClass('o_hidden');
            this.$el.find('.o_main').removeClass('o_hidden');
            this.$el.find('.flectra_app_switcher').addClass('o_hidden');
            this.$el.find('.o_menu_sections').removeClass('o_hidden');
            this.$el.find('.o_menu_brand').removeClass('o_hidden');
            this.$el.find('.o_mobile_menu_toggle').removeClass('o_hidden');
            this._updatePrevHash();
            if(this._checkModel()){
                this.$el.find('.o_control_panel').removeClass('o_hidden');
            }else{
                this.$el.find('.o_control_panel').addClass('o_hidden');
            }
            this.app_dashboard_display = false;
            this.is_prev_hash = true;

        }
    },
    _updatePrevHash: function () {
        if (!this.app_dashboard_display) {
            this.prev_hash = window.location.hash;
        }
    },
    _checkModel: function () {
        if (this.action_manager.inner_action !== null) {
            if (this.action_manager.inner_action.action_descr.res_model === false) {
                return 0;
            } else {
                return 1;
            }
        }else{
            return 0;
        }
    },
    toggle_fullscreen: function(fullscreen) {
        this._super(fullscreen);
    }
});

});